EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4700 2450 4700 6550
$Comp
L Device:R_POT RV?
U 1 1 60945FE9
P 6050 2550
F 0 "RV?" V 5843 2550 50  0000 C CNN
F 1 "10k" V 5934 2550 50  0000 C CNN
F 2 "" H 6050 2550 50  0001 C CNN
F 3 "~" H 6050 2550 50  0001 C CNN
	1    6050 2550
	0    1    1    0   
$EndComp
Wire Wire Line
	6200 2550 6200 6550
Wire Wire Line
	4500 2700 6050 2700
Connection ~ 3250 6550
Wire Wire Line
	6200 6550 5600 6550
$Comp
L power:GND #PWR?
U 1 1 6094759A
P 3250 6550
F 0 "#PWR?" H 3250 6300 50  0001 C CNN
F 1 "GND" H 3255 6377 50  0000 C CNN
F 2 "" H 3250 6550 50  0001 C CNN
F 3 "" H 3250 6550 50  0001 C CNN
	1    3250 6550
	1    0    0    -1  
$EndComp
Connection ~ 4700 6550
Wire Wire Line
	3250 6550 3250 6300
Wire Wire Line
	4600 2450 4600 4000
Wire Wire Line
	3750 5500 4400 5500
$Comp
L Device:R_POT RV?
U 1 1 6097FB20
P 7150 2500
F 0 "RV?" V 6943 2500 50  0000 C CNN
F 1 "R_POT" V 7034 2500 50  0000 C CNN
F 2 "" H 7150 2500 50  0001 C CNN
F 3 "~" H 7150 2500 50  0001 C CNN
	1    7150 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 2500 7300 6550
Wire Wire Line
	7300 6550 6200 6550
Connection ~ 6200 6550
Wire Wire Line
	3450 4200 3450 4000
Wire Wire Line
	3450 4000 4600 4000
Wire Wire Line
	1850 6550 3250 6550
Wire Wire Line
	4400 5500 4400 2450
$Comp
L MCU_Module:Arduino_UNO_R3 A?
U 1 1 6093A191
P 3250 5200
F 0 "A?" H 3250 6381 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 3250 6290 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 3250 5200 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 3250 5200 50  0001 C CNN
	1    3250 5200
	1    0    0    -1  
$EndComp
$Comp
L Comp:Screen U?
U 1 1 60996A85
P 17050 2500
F 0 "U?" V 18038 1922 50  0000 R CNN
F 1 "Screen" V 17947 1922 50  0000 R CNN
F 2 "" H 17750 6400 50  0001 C CNN
F 3 "" H 17750 6400 50  0001 C CNN
	1    17050 2500
	0    -1   -1   0   
$EndComp
$Comp
L MCU_Module:Arduino_UNO_R3 A?
U 1 1 6099D10A
P 16450 4900
F 0 "A?" V 16404 5944 50  0000 L CNN
F 1 "Arduino_UNO_R3" V 16495 5944 50  0000 L CNN
F 2 "Module:Arduino_UNO_R3" H 16450 4900 50  0001 C CIN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 16450 4900 50  0001 C CNN
	1    16450 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	16650 2900 16650 4400
Wire Wire Line
	16550 2900 16550 4400
Wire Wire Line
	16450 2900 16450 4400
Wire Wire Line
	16350 2900 16350 4400
Wire Wire Line
	16250 2900 16250 4400
Wire Wire Line
	16150 2900 16150 4400
Wire Wire Line
	16050 2900 16050 4400
Wire Wire Line
	15950 2900 15950 4400
Wire Wire Line
	4500 2450 4500 2700
Wire Wire Line
	16950 2900 16950 3600
Wire Wire Line
	16950 3600 17250 3600
Wire Wire Line
	17250 3600 17250 5000
Wire Wire Line
	3750 5600 4200 5600
Wire Wire Line
	4200 5600 4200 2450
Wire Wire Line
	16450 5000 17250 5000
Wire Wire Line
	16750 2900 16750 3700
Wire Wire Line
	16750 3700 17150 3700
Wire Wire Line
	17150 3700 17150 4850
Wire Wire Line
	15850 2900 15850 4100
Wire Wire Line
	15850 4100 15650 4100
Wire Wire Line
	15750 2900 15750 3900
Wire Wire Line
	15750 3900 15550 3900
Wire Wire Line
	15550 3900 15550 4950
Wire Wire Line
	15550 4950 16150 4950
$Comp
L power:GND #PWR?
U 1 1 60A04B37
P 19050 6450
F 0 "#PWR?" H 19050 6200 50  0001 C CNN
F 1 "GND" H 19055 6277 50  0000 C CNN
F 2 "" H 19050 6450 50  0001 C CNN
F 3 "" H 19050 6450 50  0001 C CNN
	1    19050 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	14700 4900 14700 6300
Wire Wire Line
	19450 6300 19450 3000
Wire Wire Line
	19450 3000 17250 3000
Wire Wire Line
	17250 3000 17250 2900
Wire Wire Line
	14700 4900 15350 4900
Wire Wire Line
	19050 6450 19050 6300
Wire Wire Line
	14700 6300 19050 6300
Connection ~ 19050 6300
Wire Wire Line
	19050 6300 19450 6300
Wire Wire Line
	17450 5100 18450 5100
Wire Wire Line
	18450 5100 18450 3200
Wire Wire Line
	18450 3200 17150 3200
Wire Wire Line
	17150 3200 17150 2900
$Comp
L Device:R_POT RV?
U 1 1 60A1B142
P 19000 1800
F 0 "RV?" V 18793 1800 50  0000 C CNN
F 1 "10k" V 18884 1800 50  0000 C CNN
F 2 "" H 19000 1800 50  0001 C CNN
F 3 "~" H 19000 1800 50  0001 C CNN
	1    19000 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	19150 1800 19450 1800
Connection ~ 19450 3000
Wire Wire Line
	15550 2900 15550 1800
Wire Wire Line
	15550 1800 18850 1800
Wire Wire Line
	16850 2900 16850 2400
Wire Wire Line
	16850 2400 19450 2400
Wire Wire Line
	19450 1800 19450 2400
Connection ~ 19450 2400
Wire Wire Line
	19450 2400 19450 3000
Connection ~ 14700 4900
$Comp
L Device:R_POT RV?
U 1 1 60A38352
P 13550 2550
F 0 "RV?" H 13481 2596 50  0000 R CNN
F 1 "R_POT" H 13481 2505 50  0000 R CNN
F 2 "" H 13550 2550 50  0001 C CNN
F 3 "~" H 13550 2550 50  0001 C CNN
	1    13550 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	13550 1200 13550 2400
Wire Wire Line
	13550 4900 14700 4900
Wire Wire Line
	13550 2700 13550 4900
Wire Wire Line
	15450 2900 15450 2550
Wire Wire Line
	15450 2550 13700 2550
Wire Wire Line
	17050 2150 19000 2150
Wire Wire Line
	19000 2150 19000 1950
Wire Wire Line
	17050 2150 17050 2900
Wire Wire Line
	14700 3100 15350 3100
Wire Wire Line
	15350 3100 15350 2900
Wire Wire Line
	14700 3100 14700 4900
Wire Wire Line
	16350 4850 16350 5400
Wire Wire Line
	16350 4850 17150 4850
Wire Wire Line
	16450 5000 16450 5400
Wire Wire Line
	16250 4850 16250 5400
Wire Wire Line
	15650 4850 16250 4850
Wire Wire Line
	16150 4950 16150 5400
Wire Wire Line
	15650 4100 15650 4500
Wire Wire Line
	18450 3200 19750 3200
Wire Wire Line
	19750 3200 19750 1200
Wire Wire Line
	13550 1200 19750 1200
Connection ~ 18450 3200
Connection ~ 15650 4500
Wire Wire Line
	15650 4500 15650 4850
$Comp
L Comp:Pololu_A-Star_328PB_Micro U?
U 1 1 60BE8245
P -3650 7150
F 0 "U?" V -3700 7150 50  0000 L CNN
F 1 "Pololu_A-Star_328PB_Micro" V -3600 7150 50  0000 L CNN
F 2 "" H -4100 7050 50  0001 C CNN
F 3 "" H -4100 7050 50  0001 C CNN
	1    -3650 7150
	0    1    1    0   
$EndComp
$Comp
L Comp:Screen U?
U 1 1 60BFDD0C
P -3850 5750
F 0 "U?" V -2862 5172 50  0000 R CNN
F 1 "Screen" V -2953 5172 50  0000 R CNN
F 2 "" H -3150 9650 50  0001 C CNN
F 3 "" H -3150 9650 50  0001 C CNN
	1    -3850 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	-4950 6150 -4950 6550
Wire Wire Line
	-4850 6150 -4850 6550
Wire Wire Line
	-4750 6150 -4750 6550
Wire Wire Line
	-4650 6150 -4650 6550
Wire Wire Line
	-4550 6150 -4550 6550
Wire Wire Line
	-4450 6150 -4450 6550
Wire Wire Line
	-4350 6150 -4350 6550
Wire Wire Line
	-4250 6150 -4250 6550
$Comp
L power:+5V #PWR?
U 1 1 60C6A223
P -1200 4500
F 0 "#PWR?" H -1200 4350 50  0001 C CNN
F 1 "+5V" H -1185 4673 50  0000 C CNN
F 2 "" H -1200 4500 50  0001 C CNN
F 3 "" H -1200 4500 50  0001 C CNN
	1    -1200 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	-1200 7350 -3850 7350
$Comp
L power:GND #PWR?
U 1 1 60C7C320
P -1400 7900
F 0 "#PWR?" H -1400 7650 50  0001 C CNN
F 1 "GND" H -1395 7727 50  0000 C CNN
F 2 "" H -1400 7900 50  0001 C CNN
F 3 "" H -1400 7900 50  0001 C CNN
	1    -1400 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3650 6150 -1400 6150
Wire Wire Line
	-1400 6150 -1400 7450
Wire Wire Line
	-3750 6150 -3750 6300
Wire Wire Line
	-3750 6300 -1200 6300
Wire Wire Line
	-1200 6300 -1200 7350
$Comp
L Device:R_POT RV?
U 1 1 60C88F2C
P -2200 5000
F 0 "RV?" V -2407 5000 50  0000 C CNN
F 1 "10k" V -2316 5000 50  0000 C CNN
F 2 "" H -2200 5000 50  0001 C CNN
F 3 "~" H -2200 5000 50  0001 C CNN
	1    -2200 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	-2050 5000 -1400 5000
Wire Wire Line
	-3850 6150 -3850 5400
Wire Wire Line
	-3850 5400 -2200 5400
Wire Wire Line
	-2200 5400 -2200 5150
Wire Wire Line
	-2350 5000 -5350 5000
Wire Wire Line
	-5350 5000 -5350 6150
Wire Wire Line
	-1400 5000 -1400 6150
Connection ~ -1400 6150
Wire Wire Line
	-3950 6150 -3950 6350
Wire Wire Line
	-3950 6350 -3750 6350
Wire Wire Line
	-3750 6350 -3750 7100
Wire Wire Line
	-3750 7100 -4150 7100
Wire Wire Line
	-4150 7100 -4150 7350
Wire Wire Line
	-4050 6150 -4050 6400
Wire Wire Line
	-4050 6400 -3850 6400
Wire Wire Line
	-3850 6400 -3850 6550
Wire Wire Line
	-3950 7350 -3950 7450
Wire Wire Line
	-3950 7450 -1400 7450
Connection ~ -1400 7450
Wire Wire Line
	-1400 7450 -1400 7700
Wire Wire Line
	-4150 6150 -4150 6450
Wire Wire Line
	-4150 6450 -4200 6450
Wire Wire Line
	-4200 6450 -4200 7050
Wire Wire Line
	-4200 7050 -4250 7050
Wire Wire Line
	-4250 7050 -4250 7350
Wire Wire Line
	-5050 6150 -5050 6950
Wire Wire Line
	-5050 6950 -4750 6950
Wire Wire Line
	-4750 6950 -4750 7350
Wire Wire Line
	-5150 6150 -5150 7000
Wire Wire Line
	-5150 7000 -4850 7000
Wire Wire Line
	-4850 7000 -4850 7350
Connection ~ -1400 7700
Wire Wire Line
	-1400 7700 -1400 7900
Wire Wire Line
	-5550 6150 -5550 7700
Connection ~ -5550 7700
Wire Wire Line
	-5550 7700 -1400 7700
Wire Wire Line
	-5450 6150 -5450 5550
Wire Wire Line
	-7650 7700 -5550 7700
Wire Wire Line
	-1200 4500 -1200 4550
Connection ~ -1200 6300
Connection ~ -1200 4550
Wire Wire Line
	-1200 4550 -1200 6300
Wire Wire Line
	-7750 4550 -1200 4550
Text GLabel -850 6900 2    50   Input ~ 0
USB_RX
Text GLabel -850 7000 2    50   Input ~ 0
USB_TX
Wire Wire Line
	-850 6900 -4050 6900
Wire Wire Line
	-4050 6900 -4050 6550
Wire Wire Line
	-850 7000 -4150 7000
Wire Wire Line
	-4150 7000 -4150 6550
Wire Wire Line
	-7650 5200 -7650 5400
Wire Wire Line
	-7750 4800 -7750 4550
$Comp
L Switch:SW_SPDT SW?
U 1 1 60E1E998
P -7650 5000
F 0 "SW?" V -7604 4812 50  0000 R CNN
F 1 "SW_SPDT" V -7695 4812 50  0000 R CNN
F 2 "" H -7650 5000 50  0001 C CNN
F 3 "~" H -7650 5000 50  0001 C CNN
	1    -7650 5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	-7650 7700 -7650 5700
Wire Wire Line
	-5450 5550 -7500 5550
$Comp
L Device:R_POT RV?
U 1 1 60CF300D
P -7650 5550
F 0 "RV?" H -7719 5596 50  0000 R CNN
F 1 "R_POT" H -7719 5505 50  0000 R CNN
F 2 "" H -7650 5550 50  0001 C CNN
F 3 "~" H -7650 5550 50  0001 C CNN
	1    -7650 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2450 4300 2900
Wire Wire Line
	4300 2900 5600 2900
Wire Wire Line
	5600 2900 5600 6550
Connection ~ 5600 6550
Wire Wire Line
	5600 6550 4700 6550
$Comp
L Comp:Screen U?
U 1 1 6093CF62
P 4500 2050
F 0 "U?" V 4988 -78 50  0000 R CNN
F 1 "Screen" V 4897 -78 50  0000 R CNN
F 2 "" H 4800 2200 50  0001 C CNN
F 3 "" H 4800 2200 50  0001 C CNN
	1    4500 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 6550 4700 6550
Wire Wire Line
	2750 5400 2450 5400
Wire Wire Line
	2450 5400 2450 2800
Wire Wire Line
	2450 2800 4100 2800
Wire Wire Line
	4100 2800 4100 2450
Wire Wire Line
	4000 2450 4000 2700
Wire Wire Line
	2300 2700 2300 5500
Wire Wire Line
	2300 5500 2750 5500
Wire Wire Line
	2300 2700 4000 2700
Wire Wire Line
	3900 2450 3900 2650
Wire Wire Line
	3900 2650 2150 2650
Wire Wire Line
	2150 2650 2150 5600
Wire Wire Line
	2150 5600 2750 5600
Wire Wire Line
	2750 5700 2000 5700
Wire Wire Line
	2000 5700 2000 2550
Wire Wire Line
	2000 2550 3800 2550
Wire Wire Line
	3800 2550 3800 2450
Wire Wire Line
	2750 5000 2650 5000
Wire Wire Line
	2650 5000 2650 3250
Wire Wire Line
	2650 3250 3700 3250
Wire Wire Line
	3700 3250 3700 2450
Wire Wire Line
	3600 2450 3600 3200
Wire Wire Line
	3600 3200 2600 3200
Wire Wire Line
	2600 3200 2600 5100
Wire Wire Line
	2600 5100 2750 5100
Wire Wire Line
	3500 2450 3500 3150
Wire Wire Line
	3500 3150 2550 3150
Wire Wire Line
	2550 3150 2550 5200
Wire Wire Line
	2550 5200 2750 5200
Wire Wire Line
	2750 5300 2500 5300
Wire Wire Line
	2500 5300 2500 3100
Wire Wire Line
	2500 3100 3400 3100
Wire Wire Line
	3400 3100 3400 2450
Wire Wire Line
	3300 2450 3300 3700
Wire Wire Line
	3300 3700 3950 3700
Wire Wire Line
	3950 3700 3950 5200
Wire Wire Line
	3950 5200 3750 5200
Wire Wire Line
	3200 2450 3200 3750
Wire Wire Line
	3200 3750 4000 3750
Wire Wire Line
	4000 3750 4000 5300
Wire Wire Line
	4000 5300 3750 5300
Wire Wire Line
	3100 2450 3100 4000
Wire Wire Line
	3100 4000 3450 4000
Connection ~ 3450 4000
Wire Wire Line
	3000 2450 3000 2950
Wire Wire Line
	3000 2950 5900 2950
Wire Wire Line
	5900 2950 5900 2550
Wire Wire Line
	2900 2450 2900 3000
Wire Wire Line
	2900 3000 7150 3000
Wire Wire Line
	7150 2650 7150 3000
Wire Wire Line
	2800 2450 1850 2450
Wire Wire Line
	1850 2450 1850 6550
$Comp
L Switch:SW_Push SW?
U 1 1 617ADE31
P 950 5000
F 0 "SW?" V 996 4952 50  0000 R CNN
F 1 "SW_Push" V 905 4952 50  0000 R CNN
F 2 "" H 950 5200 50  0001 C CNN
F 3 "~" H 950 5200 50  0001 C CNN
	1    950  5000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2750 4800 1400 4800
Wire Wire Line
	950  5200 950  5450
Wire Wire Line
	950  6550 1850 6550
Connection ~ 1850 6550
$Comp
L Device:C_Small C?
U 1 1 6181CFAF
P 1400 5000
F 0 "C?" H 1492 5046 50  0000 L CNN
F 1 "100nF" H 1492 4955 50  0000 L CNN
F 2 "" H 1400 5000 50  0001 C CNN
F 3 "~" H 1400 5000 50  0001 C CNN
	1    1400 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 4800 1400 4900
Connection ~ 1400 4800
Wire Wire Line
	1400 4800 950  4800
Wire Wire Line
	1400 5100 1400 5450
Wire Wire Line
	1400 5450 950  5450
Connection ~ 950  5450
Wire Wire Line
	950  5450 950  6550
Wire Wire Line
	2750 4900 2700 4900
Wire Wire Line
	2700 4900 2700 3850
Wire Wire Line
	2700 3850 7000 3850
Wire Wire Line
	7000 3850 7000 2500
$EndSCHEMATC
