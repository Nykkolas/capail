#ifdef UNIT_TEST
    #include "ArduinoFake.h"
#else
    #include "Arduino.h"
#endif
#include <Display.h>
#include <U8g2lib.h>
#include <MyButton.h>
#include <TinyGPSPlus.h>
#include <DrawPrimitives.h>
#include <GPSData.h>

TinyGPSPlus gps;
TinyGPSCustom nextPointBearingTrue;
TinyGPSCustom nextPointRange;
GPSData gpsData;
U8G2_KS0108_128X64_1 lcdScreen(U8G2_R0, 8, 9, 10, 11, 4, 5, 6, 7, /*enable=*/ A4, /*dc=*/ A3, /*cs0=*/ A0, /*cs1=*/ A1, /*cs2=*/ U8X8_PIN_NONE, /* reset=*/  U8X8_PIN_NONE); 	// Set R/W to low!
DisplayOnScreen displayOnScreen(&lcdScreen);
DisplayStruct display;
MyButton displayModeBtn (DISPLAYMODE_BTN, NORMAL_UP, 50);

void setup() {
  Serial.begin(9600);
  gpsData = gpsDataCreate ();
  nextPointBearingTrue.begin(gps, "ECRMB", 11);
  nextPointRange.begin (gps, "ECRMB", 10);
  
  lcdScreen.begin();
  display = refreshScreen (displayCreate (&displayOnScreen), gpsData);

  /** Rétro éclairage **/
  pinMode(BACKLIGHT_BTN, INPUT_PULLUP);
  pinMode(BACKLIGHT_PIN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(BACKLIGHT_BTN), switchBackLight, FALLING);

}

void loop() {
  if (Serial.available() > 0) {
        gps.encode(Serial.read());
    }

  gpsData = gpsDataUpdateFromGPS (gpsData, &gps, &nextPointBearingTrue, &nextPointRange);

  if (displayModeBtn.readFallingClick ()) {
    display = refreshScreen (cycleDisplayMode(display), gpsData);
  } else {
    display = displayData (display, gpsData);
  }
}
