#include <Display.h>

DisplayStruct displayCreate (DisplayInterface *interface) {
    DisplayStruct d;

    d.mode = NAVIGATION;
    d.lastDisplayed = 0.0;
    d.interface = interface;

    return d;
}

DisplayStruct refreshScreen (const DisplayStruct& disp, const GPSData& data) {
    DisplayStruct d = disp;
    d.lastDisplayed = millis();
            
    switch (disp.mode)
    {
    case NAVIGATION:
        d.interface->drawNavigation(data.SOG, data.CMG, data.nextPointBearingTrue, data.nextPointRange);
        break;
    case POSITION:
        d.interface->drawPosition(data.lat, data.lng);
        break;
    default:
        break;
    }

    return d;
}

DisplayStruct displayData (const DisplayStruct& disp, const GPSData& data) {
    DisplayStruct d = disp;
    
    if (data.updated) { 
        if (millis() - disp.lastDisplayed >= displayDelay || disp.lastDisplayed == 0) {
            d = refreshScreen (disp, data);
        } 
    };

    return d;
}

void switchBackLight () {
  int backLightState;

  backLightState = digitalRead(BACKLIGHT_PIN);
  digitalWrite(BACKLIGHT_PIN, (backLightState + 1) % 2 );
}

DisplayStruct cycleDisplayMode (const DisplayStruct &displayStruct) {
    DisplayStruct dNew = displayStruct;
    dNew.mode = (DisplayMode)((dNew.mode + 1) % DISPLAYMODESIZE);
    return dNew;
}

bool DisplayStruct::operator==(DisplayStruct other) {
    return \
        mode == other.mode &&
        lastDisplayed == other.lastDisplayed;
}