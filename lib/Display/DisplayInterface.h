#ifndef DISPLAYINTERFACE_H
#define DISPLAYINTERFACE_H

#include <GPSConst.h>

class DisplayInterface {
    public:
        virtual void drawNavigation(int SOG, int CMG, int nextPointBearingTrue, int nextPointRange) = 0;
        virtual void drawPosition(const char lat[LATITUDE_SIZE], const char lng[LONGITUDE_SIZE]) = 0;
};

#endif
