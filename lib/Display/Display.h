#ifndef DISPLAY_H
#define DISPLAY_H

#include <DisplayInterface.h>
#include <GPSData.h>

const int BACKLIGHT_BTN = 2;
const int BACKLIGHT_PIN = 12;
const int DISPLAYMODE_BTN = 13;

const unsigned long displayDelay = 200;

const int DISPLAYMODESIZE = 2;
enum DisplayMode {
    NAVIGATION,
    POSITION
};

struct DisplayStruct {
    DisplayMode mode;
    unsigned long lastDisplayed;
    DisplayInterface *interface;
    bool operator==(DisplayStruct other);
};

DisplayStruct displayCreate (DisplayInterface *_interface);
DisplayStruct refreshScreen (const DisplayStruct& disp, const GPSData& data);
DisplayStruct displayData (const DisplayStruct& disp, const GPSData& data);
void switchBackLight ();
DisplayStruct cycleDisplayMode (const DisplayStruct& d);

#endif