#include <DrawPrimitives.h>
#include <U8g2lib.h>

void drawNavigationTemplate(U8G2_KS0108_128X64_1 lcdScreen) {
    lcdScreen.drawVLine(HSection, 0, 64);
    lcdScreen.drawHLine(0, VSection, 128);
}

void drawSpeed(U8G2_KS0108_128X64_1 lcdScreen, int speedValue) {
    char buf[6];
    u8g2_uint_t dataWidth;
    
    // Valeur
    sprintf(buf, "%02d", speedValue);
    lcdScreen.setFont(u8g2_font_ncenB24_tn);
    dataWidth = lcdScreen.drawStr(0 + 4 * HPadding, VSection - VPadding, buf);
    // "kn"
    lcdScreen.setFont(u8g2_font_ncenB08_tr);
    lcdScreen.drawStr(dataWidth + 4 * HPadding, VSection - VPadding, "kn");
}

void drawCMG(U8G2_KS0108_128X64_1 lcdScreen, int cmgValue) {
    char buf[7];
    u8g2_uint_t dataWidth;
    
    // Valeur
    sprintf(buf, "%03d\xB0", cmgValue);
    lcdScreen.setFont(u8g2_font_ncenB24_tn);
    dataWidth = lcdScreen.drawStr(HSection + HPadding, VSection - VPadding, buf);
    // "°"
    lcdScreen.setFont(u8g2_font_ncenB14_tf);
    lcdScreen.drawGlyph(dataWidth + HSection, 20 + VPadding, 176);
    /** Petite boussole pour nord magnétique
    lcdScreen.setFont(u8g2_font_open_iconic_app_1x_t);
    lcdScreen.drawGlyph(dataWidth + HSection, VSection - VPadding, 70); 
    */
   // Icône
    lcdScreen.setFont(u8g2_font_open_iconic_www_1x_t);
    lcdScreen.drawGlyph(dataWidth + HSection, VSection - VPadding, 78); 

}

void drawNextPointBearingTrue(U8G2_KS0108_128X64_1 lcdScreen, int nextPointBearingTrueValue) {
    char buf[6];
    u8g2_uint_t dataWidth;
    
    // Valeur
    if (nextPointBearingTrueValue < 0) {
        lcdScreen.setFont(u8g2_font_courB24_tn);
        lcdScreen.drawStr(HSection + HPadding, Height - VPadding, "---");
    } else {
        sprintf(buf, "%03d", nextPointBearingTrueValue);
        lcdScreen.setFont(u8g2_font_ncenB24_tn);
        dataWidth = lcdScreen.drawStr(HSection + HPadding, Height - VPadding, buf);
        // "°"
        lcdScreen.setFont(u8g2_font_ncenB14_tf);
        lcdScreen.drawGlyph(dataWidth + HSection, VSection + VPadding + 21, 176);
        // Icône
        lcdScreen.setFont(u8g2_font_open_iconic_www_1x_t);
        lcdScreen.drawGlyph(dataWidth + HSection, Height - VPadding, 71); 
    }
}

void drawNextPointRange(U8G2_KS0108_128X64_1 lcdScreen, int nextPointRangeValue) {
    char buf[6];
    u8g2_uint_t dataWidth;
    
    // Valeur
    if (nextPointRangeValue < 0) {
        lcdScreen.setFont(u8g2_font_courB24_tn);
        lcdScreen.drawStr(0 + HPadding, Height - VPadding, "---");
    } else {
        sprintf(buf, "%02d", nextPointRangeValue);
        lcdScreen.setFont(u8g2_font_ncenB24_tn);
        dataWidth = lcdScreen.drawStr(0 + 4 * HPadding, Height - VPadding, buf);
        // "mn"
        lcdScreen.setFont(u8g2_font_ncenB08_tr);
        lcdScreen.drawStr(dataWidth + 4 * HPadding, Height - VPadding, "mn");
    }
}

void drawPositionTemplate(U8G2_KS0108_128X64_1 lcdScreen) {
    lcdScreen.drawHLine(0, VSection, 128);
}

void drawLat(U8G2_KS0108_128X64_1 lcdScreen, const char *lat) {
    // Valeur
    lcdScreen.setFont(u8g2_font_ncenB14_tf);
    lcdScreen.drawStr(0, VSection - VPadding, lat);
}

void drawLng(U8G2_KS0108_128X64_1 lcdScreen, const char *lng) {
    // Valeur
    lcdScreen.setFont(u8g2_font_ncenB14_tf);
    lcdScreen.drawStr(0, Height - VPadding, lng);
}

DisplayOnScreen::DisplayOnScreen(U8G2_KS0108_128X64_1* _lcdScreen) {
    lcdScreen = _lcdScreen;
}
    
void DisplayOnScreen::drawNavigation(int SOG, int CMG, int nextPointBearingTrue, int nextPointRange) {
    lcdScreen->firstPage();
    do {
        drawNavigationTemplate(*lcdScreen);
        drawSpeed(*lcdScreen, SOG);
        drawCMG(*lcdScreen, CMG);
        drawNextPointBearingTrue(*lcdScreen, nextPointBearingTrue);
        drawNextPointRange (*lcdScreen, nextPointRange);
    
    } while (lcdScreen->nextPage());
}

void DisplayOnScreen::drawPosition(const char lat[LATITUDE_SIZE], const char lng[LONGITUDE_SIZE]) {
    lcdScreen->firstPage();
    do {
        drawPositionTemplate(*lcdScreen);
        drawLat (*lcdScreen, lat);
        drawLng (*lcdScreen, lng);
    } while (lcdScreen->nextPage());
}