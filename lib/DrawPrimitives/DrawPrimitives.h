#ifndef DRAWPRIMITIVES_H
#define DRAWPRIMITIVES_H

#include <U8g2lib.h>
#include <DisplayInterface.h>
#include <GPSConst.h>

const u8g2_uint_t Width = 127;
const u8g2_uint_t Height = 63;
const u8g2_uint_t VSection = 31;
const u8g2_uint_t HSection = 63;
const u8g2_uint_t VPadding = 1;
const u8g2_uint_t HPadding = 1;

void drawNavigationTemplate(U8G2_KS0108_128X64_1 lcdScreen);
void drawSpeed(U8G2_KS0108_128X64_1 lcdScreen, int speedValue);
void drawCMG(U8G2_KS0108_128X64_1 lcdScreen, int cmgValue);
void drawNextPointBearingTrue(U8G2_KS0108_128X64_1 lcdScreen, int nextPointBearingTrueValue);
void drawNextPointRange(U8G2_KS0108_128X64_1 lcdScreen, int nextPointRangeValue);

void drawPositionTemplate(U8G2_KS0108_128X64_1 lcdScreen);
void drawLat(U8G2_KS0108_128X64_1 lcdScreen, char *lat);
void drawLng(U8G2_KS0108_128X64_1 lcdScreen, char *lng);

class DisplayOnScreen : public DisplayInterface {
    private:
        U8G2_KS0108_128X64_1 *lcdScreen;
    public:
        DisplayOnScreen(U8G2_KS0108_128X64_1 *_lcdScreen);
        void drawNavigation(int SOG, int CMG, int nextPointBearingTrue, int nextPointRange);
        void drawPosition(const char lat[LATITUDE_SIZE], const char lng[LONGITUDE_SIZE]);
};

#endif
