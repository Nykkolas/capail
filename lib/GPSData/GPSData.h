#ifndef GPSDATA_H
#define GPSDATA_H

#include <GPSConst.h>
#include <TinyGPSPlus.h>

const unsigned int maxDataAge = 2000;

struct GPSData {
    bool updated;
    int SOG;
    int CMG;
    int nextPointBearingTrue;
    int nextPointRange;

    char lat[LATITUDE_SIZE];
    char lng[LONGITUDE_SIZE];

    bool operator==(const GPSData& other) const;
};

GPSData gpsDataCreate();
GPSData gpsDataCreate(const bool updated, const int SOG, const int CMG, const int nextnextPointBearingTrue, const int nextPointRange, const char lat[LATITUDE_SIZE], const char lng[LONGITUDE_SIZE]);
GPSData gpsDataUpdateFromGPS (const GPSData& navData, TinyGPSPlus *gps, TinyGPSCustom *nextPointBearingTrue, TinyGPSCustom *nextPointRange);
GPSData gpsDataSetSOG (const GPSData& d, int sog);

#endif