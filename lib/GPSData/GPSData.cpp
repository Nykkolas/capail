#include <GPSData.h>

GPSData gpsDataCreate () {
    GPSData gpsData;

    gpsData.updated = false;
    gpsData.SOG = 0;
    gpsData.CMG = 0;
    gpsData.nextPointBearingTrue = -1;
    gpsData.nextPointRange = -1;

    sprintf (gpsData.lat, "00\xB0%s", "00.0000'N");
    sprintf (gpsData.lng, "000\xB0%s", "00.0000'E");
    
    return gpsData;
};

GPSData gpsDataCreate(\
        const bool updated, 
        const int SOG, 
        const int CMG, 
        const int nextPointBearingTrue,  
        const int nextPointRange, 
        const char lat[LATITUDE_SIZE], 
        const char lng[LONGITUDE_SIZE]) {

    GPSData gpsData;

    gpsData.updated = updated;
    gpsData.SOG = SOG;
    gpsData.CMG = CMG;
    gpsData.nextPointBearingTrue = nextPointBearingTrue;
    gpsData.nextPointRange = nextPointRange;

    strncpy (gpsData.lat, lat, LATITUDE_SIZE);
    strncpy (gpsData.lng, lng, LONGITUDE_SIZE);
    
    return gpsData;
}

GPSData gpsDataUpdateFromGPS (
        const GPSData &navData, \
        TinyGPSPlus *gps, \
        TinyGPSCustom *nextPointBearingTrue,\
        TinyGPSCustom *nextPointRange) {
    
    GPSData g = navData;
    g.updated = false;

    if (gps->course.isUpdated() && gps->course.isValid()) {
        g.CMG = (int)round(gps->course.deg());
        g.updated = true;
    }

    if (gps->speed.isUpdated() && gps->speed.isValid()) {
        g = gpsDataSetSOG(g, (int)round(gps->speed.knots()));
        g.updated = true;
    }

    if (nextPointBearingTrue->isUpdated()) {
        g.nextPointBearingTrue = (int)round(atof(nextPointBearingTrue->value()));
        g.updated = true;
    } else if (nextPointBearingTrue->age() > maxDataAge && g.nextPointBearingTrue >= 0) {
        g.nextPointBearingTrue = -1;
        g.updated = true;
    }
    
    if (nextPointRange->isUpdated()) {
        g.nextPointRange = (int)round(atof(nextPointRange->value()));
        g.updated = true;
    } else if (nextPointRange->age() > maxDataAge && g.nextPointRange >= 0) {
        g.nextPointRange = -1;
        g.updated = true;
    }
    
    if (gps->location.isUpdated() && gps->location.isValid()) {
        char latMinutes[9];
        char lngMinutes[9];

        char NorS = gps->location.rawLat().negative ? 'S' : 'N';
        char EorO = gps->location.rawLng().negative ? 'O' : 'E';

        // Latitude
        dtostrf ((abs(gps->location.lat()) - gps->location.rawLat().deg) * 60, -1, 4, latMinutes);
        sprintf (g.lat, "%02d\xB0%s'%c", gps->location.rawLat().deg, latMinutes, NorS);

        // Longitude
        dtostrf ((abs(gps->location.lng()) - gps->location.rawLng().deg) * 60, -1, 4, lngMinutes);
        sprintf (g.lng, "%03d\xB0%s'%c", gps->location.rawLng().deg, lngMinutes, EorO);

        g.updated = true;
    }

    return g;
}

GPSData gpsDataSetSOG (const GPSData& d, int sog) {
    GPSData d2 = d;
    d2.SOG = sog;

    return d2;
}

bool GPSData::operator==(const GPSData& other) const {
    return \
        updated == other.updated &&
        SOG == other.SOG &&
        CMG == other.CMG &&
        nextPointBearingTrue == other.nextPointBearingTrue &&
        nextPointRange == other.nextPointRange &&
        (strcmp (lat, other.lat) == 0) &&
        (strcmp (lng, other.lng) == 0);
}
    