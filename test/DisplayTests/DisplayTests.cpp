
#include <ArduinoFake.h>
#include <unity.h>
#include <Display.h>
#include <DisplayInterface.h>
#include <GPSData.h>

using namespace fakeit;

/** Helper functions and classes **/
class DummyInterface : public DisplayInterface {
    public:
        bool drawNavigationCalled = false;
        bool drawPositionCalled = false;
        void drawNavigation(int SOG, int CMG, int nextPointBearingTrue, int nextPointRange) { drawNavigationCalled = true; };
        void drawPosition(const char lat[LATITUDE_SIZE], const char lng[LONGITUDE_SIZE]) { drawPositionCalled = true; };
};

/** Tests **/
void setUp(void)
{
    ArduinoFakeReset();
}

void test_operatorEquality_equal(void) {
    DisplayStruct disp;
    DisplayStruct dispOther;
    disp.mode = NAVIGATION;
    disp.lastDisplayed = 0.0;

    dispOther.mode = NAVIGATION;
    dispOther.lastDisplayed = 0.0;

    TEST_ASSERT_TRUE (disp == dispOther);
}

void test_displayCreate_Ok (void) {
    DisplayStruct disp;
    DisplayStruct dispExpected;
    DummyInterface dummyInterface;

    dispExpected.lastDisplayed = 0;
    dispExpected.mode = NAVIGATION;

    disp = displayCreate(&dummyInterface);

    TEST_ASSERT_TRUE(disp == dispExpected);
}

void test_refreshScreen_navigationMode (void) {
    DisplayStruct disp;
    DummyInterface dummyInterface;
    GPSData gpsData = gpsDataCreate();
    DisplayStruct dispExpected;

    dispExpected.lastDisplayed = 100;
    dispExpected.mode = NAVIGATION;

    When(Method(ArduinoFake(), millis)).AlwaysReturn(100);

    disp = refreshScreen (displayCreate(&dummyInterface), gpsData);

    TEST_ASSERT_TRUE_MESSAGE(disp == dispExpected, "lastDisplayed a été mis à jour");
    TEST_ASSERT_TRUE_MESSAGE(dummyInterface.drawNavigationCalled, "drawNavigation a été appelé");
}

void test_refreshScreen_positionMode (void) {
    DisplayStruct disp;
    DummyInterface dummyInterface;
    GPSData gpsData = gpsDataCreate();
    DisplayStruct dispExpected;

    dispExpected.lastDisplayed = 100;
    dispExpected.mode = POSITION;

    When(Method(ArduinoFake(), millis)).AlwaysReturn(100);
    
    disp = displayCreate(&dummyInterface);
    disp.mode = POSITION;
    disp = refreshScreen (disp, gpsData);

    TEST_ASSERT_TRUE_MESSAGE(disp == dispExpected, "lastDisplayed a été mis à jour");
    TEST_ASSERT_TRUE_MESSAGE(dummyInterface.drawPositionCalled, "drawPosition a été appelé");
}

void test_displayData_lessThanDisplayDelay (void) {
    DummyInterface dummyInterface;
    DisplayStruct disp;
    GPSData gpsData = gpsDataCreate();
    const int delay = 2002;

    gpsData.updated = true;
    disp = displayCreate (&dummyInterface);
    disp.lastDisplayed = 2000;

    When(Method(ArduinoFake(), millis)).Return(delay);

    disp = displayData (disp, gpsData);

    TEST_ASSERT_EQUAL_MESSAGE (2000, disp.lastDisplayed, "Le délais n'a pas été mis à jour");
    TEST_ASSERT_FALSE_MESSAGE (dummyInterface.drawNavigationCalled, "drawNavigation n'a pas été appelé");
}

void test_displayData_moreThanDisplayDelay (void) {
    DummyInterface dummyInterface;
    DisplayStruct disp;
    GPSData gpsData = gpsDataCreate();
    const int delay = 2201;

    gpsData.updated = true;
    disp = displayCreate (&dummyInterface);
    disp.lastDisplayed = 2000;

    When(Method(ArduinoFake(), millis)).AlwaysReturn(delay);

    disp = displayData (disp, gpsData);

    TEST_ASSERT_EQUAL_MESSAGE (delay, disp.lastDisplayed, "Le délais a été mis à jour");
    TEST_ASSERT_TRUE_MESSAGE (dummyInterface.drawNavigationCalled, "drawNavigation a été appelé");
}

void test_cycleDisplayMode_switchNavToPos (void) {
    DisplayStruct disp;
    DummyInterface DummyInterface;
    DisplayStruct dispExpected;

    disp = displayCreate (&DummyInterface);

    dispExpected.mode = POSITION;

    disp = cycleDisplayMode (disp);

    TEST_ASSERT_EQUAL (dispExpected.mode, disp.mode);
}

void test_cycleDisplayMode_switchPosToNav (void) {
    DisplayStruct disp;
    DummyInterface DummyInterface;
    DisplayStruct dispExpected;

    disp = displayCreate (&DummyInterface);
    disp.mode = POSITION;

    dispExpected.mode = NAVIGATION;

    disp = cycleDisplayMode (disp);

    TEST_ASSERT_EQUAL (dispExpected.mode, disp.mode);
}

int main(int argc, char **argv)
{
    UNITY_BEGIN();

    RUN_TEST(test_operatorEquality_equal);
    RUN_TEST(test_displayCreate_Ok);
    RUN_TEST(test_refreshScreen_navigationMode);
    RUN_TEST(test_refreshScreen_positionMode);
    RUN_TEST(test_displayData_lessThanDisplayDelay);
    RUN_TEST(test_displayData_moreThanDisplayDelay);
    RUN_TEST(test_cycleDisplayMode_switchNavToPos);
    RUN_TEST(test_cycleDisplayMode_switchPosToNav);

    UNITY_END();

    return 0;
}