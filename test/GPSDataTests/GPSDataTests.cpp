#include <ArduinoFake.h>
#include <unity.h>
#include <GPSData.h>
#include <TinyGPSPlus.h>

using namespace fakeit;

/** Helper functions **/
const int encodeMillis = 100;

TinyGPSPlus encodeGPSStream (TinyGPSPlus gps, const char *gpsStream) {
    When(Method(ArduinoFake(), millis)).AlwaysReturn(encodeMillis);
    
    while (*gpsStream)
        gps.encode(*gpsStream++);

    return gps;
}

void setUp(void)
{
    ArduinoFakeReset();
}

// void tearDown(void) {
// // clean stuff up here
// }

void test_operatorEquality_lngDifferent (void) {
    char latInput[LATITUDE_SIZE] = "48\xB0" "55.4234'N";
    char lngInput[LONGITUDE_SIZE] = "002\xB0" "14.7901";
    char lngExpected[LONGITUDE_SIZE] = "002\xB0" "14.7901'E";
    GPSData gpsDataInput = gpsDataCreate (false, 5, 14, 6, 83, latInput, lngInput);
    GPSData gpsDataExpected = gpsDataCreate (false, 5, 14, 6, 83, latInput, lngExpected);

    TEST_ASSERT_FALSE(gpsDataInput == gpsDataExpected);
}

void test_gpsDataCreate_Ok(void) {
    GPSData g;

    g = gpsDataCreate ();

    TEST_ASSERT_FALSE(g.updated);
}

void test_gpsDataUpdateFromGPS_TinyGPSUpdated(void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    gps = encodeGPSStream (gps, "$GNGGA,172756.00,4855.42338,N,00214.79007,E,1,09,1.30,58.4,M,46.1,M,,*7F\r\n");

    TEST_ASSERT_TRUE_MESSAGE(gps.location.isUpdated(), "TinyGPS a vu des changements");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_FALSE_MESSAGE(gps.location.isUpdated(), "TinyGPS a vu que l'objet a été lu");
}

void test_gpsDataUpdateFromGPS_GGA (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "48\xB0" "55.4234'N";
    char expectedLng[LONGITUDE_SIZE] = "002\xB0" "14.7901'E";
    GPSData gpsDataExpected = gpsDataCreate (true, 0, 0, -1, -1, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$GNGGA,172756.00,4855.42338,N,00214.79007,E,1,09,1.30,58.4,M,46.1,M,,*7F\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_EQUAL_INT_MESSAGE (0, gps.failedChecksum(), "Pas d'echec de checksum");
    TEST_ASSERT_EQUAL_INT_MESSAGE (1, gps.passedChecksum(), "Le checksum est valide");

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Les données sont correctes");
    TEST_ASSERT_TRUE(true);
}

void test_gpsDataUpdateFromGPS_RMCNorthEst (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "48\xB0" "55.4234'N";
    char expectedLng[LONGITUDE_SIZE] = "002\xB0" "14.7902'E";
    GPSData gpsDataExpected = gpsDataCreate (true, 6, 111, -1, -1, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$GNRMC,172757.00,A,4855.42336,N,00214.79021,E,5.762,111,091021,,,A,V*2F\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_EQUAL_INT_MESSAGE (0, gps.failedChecksum(), "Pas d'echec de checksum");
    TEST_ASSERT_EQUAL_INT_MESSAGE (1, gps.passedChecksum(), "Le checksum est valide");

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Les données sont correctes");
    TEST_ASSERT_TRUE(true);
}

void test_gpsDataUpdateFromGPS_RMCSouthWest (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "48\xB0" "55.4234'S";
    char expectedLng[LONGITUDE_SIZE] = "002\xB0" "14.7902'O";
    GPSData gpsDataExpected = gpsDataCreate (true, 6, 111, -1, -1, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$GNRMC,172757.00,A,4855.42336,S,00214.79021,W,5.762,111,091021,,,A,V*20\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_EQUAL_INT_MESSAGE (0, gps.failedChecksum(), "Pas d'echec de checksum");
    TEST_ASSERT_EQUAL_INT_MESSAGE (1, gps.passedChecksum(), "Le checksum est valide");

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Les données sont correctes");
    TEST_ASSERT_TRUE(true);
}

void test_gpsDataUpdateFromGPS_RMB (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "00\xB0" "00.0000'N";
    char expectedLng[LONGITUDE_SIZE] = "000\xB0" "00.0000'E";
    GPSData gpsDataExpected = gpsDataCreate (true, 0, 000, 92, 6, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$ECRMB,A,0.001,R,001,,4855.196,N,00223.421,E,5.675,92.278,-0.037,V,A*4C\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_EQUAL_INT_MESSAGE (0, gps.failedChecksum(), "Pas d'echec de checksum");
    TEST_ASSERT_EQUAL_INT_MESSAGE (1, gps.passedChecksum(), "Le checksum est valide");

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Les données sont correctes");
    TEST_ASSERT_TRUE(true);
}

void test_gpsDataUpdateFromGPS_RMBdelayExpired (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "00\xB0" "00.0000'N";
    char expectedLng[LONGITUDE_SIZE] = "000\xB0" "00.0000'E";
    GPSData gpsDataExpected = gpsDataCreate (true, 0, 000, -1, -1, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$ECRMB,A,0.001,R,001,,4855.196,N,00223.421,E,5.675,92.278,-0.037,V,A*4C\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    When(Method(ArduinoFake(), millis)).AlwaysReturn(maxDataAge + encodeMillis + 1);

    gpsData = gpsDataUpdateFromGPS (gpsData, &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Affichage de --- et ---");
}

void test_gpsDataUpdateFromGPS_3sentences (void) {
    TinyGPSPlus gps;
    TinyGPSCustom nextPointBearingTrue;
    TinyGPSCustom nextPointRange;
    GPSData gpsData;

    char expectedLat[LATITUDE_SIZE] = "48\xB0" "55.4234'N";
    char expectedLng[LONGITUDE_SIZE] = "002\xB0" "14.7902'E";
    GPSData gpsDataExpected = gpsDataCreate (true, 6, 111, 92, 6, expectedLat, expectedLng);

    nextPointBearingTrue.begin(gps, "ECRMB", 11);
    nextPointRange.begin (gps, "ECRMB", 10);

    gps = encodeGPSStream (gps, "$GNGGA,172756.00,4855.42338,N,00214.79007,E,1,09,1.30,58.4,M,46.1,M,,*7F\r\n");
    gps = encodeGPSStream (gps, "$GNRMC,172757.00,A,4855.42336,N,00214.79021,E,5.762,111,091021,,,A,V*2F\r\n");
    gps = encodeGPSStream (gps, "$ECRMB,A,0.001,R,001,,4855.196,N,00223.421,E,5.675,92.278,-0.037,V,A*4C\r\n");

    gpsData = gpsDataUpdateFromGPS (gpsDataCreate(), &gps, &nextPointBearingTrue, &nextPointRange);

    TEST_ASSERT_EQUAL_INT_MESSAGE (0, gps.failedChecksum(), "Pas d'echec de checksum");
    TEST_ASSERT_EQUAL_INT_MESSAGE (3, gps.passedChecksum(), "Le checksum est valide");

    TEST_ASSERT_TRUE_MESSAGE (gpsData == gpsDataExpected, "Les données sont correctes");
    TEST_ASSERT_TRUE(true);
}

int main(int argc, char **argv)
{
    UNITY_BEGIN();

    RUN_TEST(test_operatorEquality_lngDifferent);
    RUN_TEST(test_gpsDataCreate_Ok);
    RUN_TEST(test_gpsDataUpdateFromGPS_TinyGPSUpdated);
    RUN_TEST(test_gpsDataUpdateFromGPS_GGA);
    RUN_TEST(test_gpsDataUpdateFromGPS_RMCNorthEst);
    RUN_TEST(test_gpsDataUpdateFromGPS_RMCSouthWest);
    RUN_TEST(test_gpsDataUpdateFromGPS_RMB);
    RUN_TEST(test_gpsDataUpdateFromGPS_RMBdelayExpired);
    RUN_TEST(test_gpsDataUpdateFromGPS_3sentences);

    UNITY_END();

    return 0;
}